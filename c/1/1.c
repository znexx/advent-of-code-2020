/**
 * Entry for December 1st of the Advent of Code 2020
 * Author: Johan Osvaldsson (znexxpwnz0r@gmail.com
 */
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define TARGET_SUM (2020)

/**
 * Reads integers from a file into a dynamically allocated integer array
 *
 * @param file The file to read integers from
 * @param numbers A pointer to the array to fill. Will be realloc'd to fit increasing amount of entries
 * @return The number of numbers successfully read, or -1 if the array pointer parameter was NULL
 */
int read_numbers (FILE *file, int **numbers) {
	if (numbers == NULL) {
		return -1;
	}

	int i = 0;
	size_t n_numbers = 0;
	while (fscanf(file, "%d", &i) > 0) {
		n_numbers++;
		(*numbers) = realloc((*numbers), n_numbers * sizeof(int));
		(*numbers)[n_numbers - 1] = i;
	}

	return n_numbers;
}

/**
 * Tests the two integers against a criterion and returns whether it is fulfilled.
 *
 * @param a The first integer to use for the test
 * @param b The second integer to use for the test
 * @return Whether the test was successful
 * @note This implementation adds a and b and checks if the result is equal to the constant TARGET_SUM
 */
bool check_match (int a, int b) {
	return a + b == TARGET_SUM;
}

int main (int argc, char **argv) {
	if (argc != 2) {
		printf("Wrong number of arguments!\n");
		printf("Usage: %s INPUT_FILE\n", argv[0]);
		return -1;
	}

	FILE *file = fopen(argv[1], "r");
	if (file == NULL) {
		fprintf(stderr, "Could not open file %s!\n", argv[1]);
		return -2;
	}

	int *numbers = NULL;
	int n_numbers = read_numbers(file, &numbers);
	fclose(file);

	if (n_numbers < 1) {
		fprintf(stderr, "Could not read numbers from %s!\n", argv[1]);
		return -3;
	}

	bool match_found = false;
	for (int a = 0; a < n_numbers - 1 && !match_found; a++) {
		for (int b = a + 1; b < n_numbers && !match_found; b++) {
			match_found = check_match(numbers[a], numbers[b]);
			if (match_found) {
				printf("%d\n", numbers[a] * numbers[b]);
			}
		}
	}

	if (!match_found) {
		printf("Found no match!\n");
	}

	free(numbers);
	return 0;
}
