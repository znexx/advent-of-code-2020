# December 1st
Here is my solution for the first day in the Advent of Code! It was quickly solved with C together with the standard library.

## Requirements
All you need is make, gcc, and them being set up as normal. There is no trickery going on, just bog-standard Linux-style compilation!

## Compiling
Just run `make`! The default target should compile, link, and run the program with the input file in the repo!
