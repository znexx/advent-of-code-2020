# December 3rd
Here is my solution for the third day in the Advent of Code! This was a bit of a different challenge, and reading formatted strings became out-of-scope for this puzzle.

## Requirements
All you need is make, gcc, and them being set up as normal. There is no trickery going on, just bog-standard Linux-style compilation!

## Compiling
Just run `make`! The default target should compile, link, and run the program with the input file in the repo!
