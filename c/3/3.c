/**
 * Entry for December 2st of the Advent of Code 2020
 * Author: Johan Osvaldsson (znexxpwnz0r@gmail.com
 */
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define DEBUG

/**
 * Calculates and returns the length of the first line of the provided file
 *
 * @param file The file to read the first line length from
 * @return The number of characters 
 */
ptrdiff_t find_width (FILE *file) {
	if (file == NULL) {
		return -1;
	}

	// saves the position indicator for the file, and rewinds it to the start
	fpos_t initial_position;
	if (fgetpos(file, &initial_position)) {
		return -2;
	} else {
		rewind(file);
	}

	int i = 0;
	while (getc(file) != '\n') i++;

	// resets the position indicator for the file
	fsetpos(file, &initial_position);

	return i - 0;
}

/**
 * Reads password lines from a file into a dynamically allocated array
 *
 * @param file The file to read lines from
 * @param lines A pointer to the array to fill. Will be realloc'd to fit increasing amount of entries
 * @return The number of numbers successfully read, or -1 if the array pointer parameter was NULL
 */
int read_grid (FILE *file, char ***grid, int *width) {
	if (grid == NULL) {
		return -1;
	}

	int line_number = -1;
	int character = '\n';
	int column = 0;

	do {
		if (character == '\n') {
			(*grid) = realloc((*grid), (++line_number + 1) * sizeof(char*));
			(*grid)[line_number] = calloc(*width, sizeof(char));
			column = 0;
			character = getc(file);
		} else if (character == EOF) {
			break;
		}
		(*grid)[line_number][column++] = character;
	} while ((character = getc(file)) != EOF);

	return line_number;
}

/**
 * Prints the slope
 *
 * @param grid The 2-dimensional array representing the slope
 * @param width The length of each sub-array in the array
 * @param height The number of sub-arrays in the array
 */
void print_slope (char **grid, int width, int height) {
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			putchar(grid[y][x]);
		}
		putchar('\n');
	}
}

/**
 * Performs a travel run
 *
 * @param grid The 2-dimensional array of chars representing the slope
 * @param width The width, or length of each sub-array, in the grid
 * @param height The height, or number of sub-arrays in the grid
 * @param dx The number of steps in the x-direction we will take each iteration
 * @param dy The number of steps in the y-direction we will take each iteration
 */
int go_travel (char **grid, int width, int height, int dx, int dy) {
	int hits = 0, misses = 0;

	int x = dx;
	for (int y = dy; (y >= 0) && (y < height); y += dy) {
		switch (grid[y][x]) {
			case '.':
				misses++;
				break;
			case '#':
				hits++;
				break;
		}
		x = (x + dx) % width;
	}

	return hits;
}

/**
 * Calculates the product of hit trees for a list of different slopes
 *
 * @param grid The 2-dimensional array of chars representing the slope
 * @param width The width, or length of each sub-array, in the grid
 * @param height The height, or number of sub-arrays in the grid
 * @return The product
 */
long hits_for_all_slopes (char **grid, int width, int height) {
	int slopes[][2] = {
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2}
	};

	long product = 1;
	int trees = 0;

	for (int i = 0; i < sizeof(slopes) / sizeof(int[2]); i++) {
		trees = go_travel(grid, width, height, slopes[i][0], slopes[i][1]);
#ifdef DEBUG
		printf("Slope: dx: %d dy: %d, Trees hit: %d\n", slopes[i][0], slopes[i][1], trees);
#endif
		product *= trees;
	}

	return product;
}

int main (int argc, char **argv) {
	if (argc != 2) {
		printf("Wrong number of arguments!\n");
		printf("Usage: %s INPUT_FILE\n", argv[0]);
		return -1;
	}

	FILE *file = fopen(argv[1], "r");
	if (file == NULL) {
		fprintf(stderr, "Could not open file %s!\n", argv[1]);
		return -2;
	}

	int width = find_width(file);

	char **grid = NULL;
	int height = read_grid(file, &grid, &width);
	fclose(file);

	if (height < 1) {
		fprintf(stderr, "Could not read the grid from %s!\n", argv[1]);
		return -3;
	}

	printf("Got width: %d and height: %d\n", width, height);

	int hit_trees = go_travel(grid, width, height, 1, 1);
#ifdef DEBUG
	print_slope(grid, width, height);
#endif
	printf("Encountered %d trees with slope dx: %d, dy: %d.\n", hit_trees, 1, 1);

	printf("Product of hit trees is: %ld\n", hits_for_all_slopes(grid, width, height));

	for (int i = 0; i < height; i++) {
		free(grid[i]);
	}
	free(grid);
	return 0;
}
