# December 2st
Here is my solution for the second day in the Advent of Code! Similary enough to the first puzzle to be easily solved with just a few modifications to the code.

In this solution, I utilize a macro called DEBUG, which if defined includes some printf-debugging statements in the code. To enable it, uncomment the line in 2.c, or run with `make debug`

## Requirements
All you need is make, gcc, and them being set up as normal. There is no trickery going on, just bog-standard Linux-style compilation!

## Compiling
Just run `make`! The default target should compile, link, and run the program with the input file in the repo!
