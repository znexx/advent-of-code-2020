/**
 * Entry for December 2st of the Advent of Code 2020
 * Author: Johan Osvaldsson (znexxpwnz0r@gmail.com
 */
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_PASSWORD_LENGTH (20)
//#define DEBUG

typedef struct {
	int min;
	int max;
	char character;
	char password[MAX_PASSWORD_LENGTH];
} Line_t;

/**
 * Reads password lines from a file into a dynamically allocated array
 *
 * @param file The file to read lines from
 * @param lines A pointer to the array to fill. Will be realloc'd to fit increasing amount of entries
 * @return The number of numbers successfully read, or -1 if the array pointer parameter was NULL
 */
int read_lines (FILE *file, Line_t **lines) {
	if (lines  == NULL) {
		return -1;
	}

	int min, max;
	char character, password[MAX_PASSWORD_LENGTH];
	size_t n_lines = 0;
	while (fscanf(file, "%d-%d %c: %s", &min, &max, &character, password) > 0) {
		n_lines++;
		(*lines) = realloc((*lines), n_lines * sizeof(Line_t));
		(*lines)[n_lines - 1].min = min;
		(*lines)[n_lines - 1].max = max;
		(*lines)[n_lines - 1].character = character;
		strcpy((*lines)[n_lines - 1].password, password);
	}

	return n_lines;
}

/**
 * Tests the two integers against the criterion in part 1 of puzzle 2 and returns whether it is fulfilled.
 *
 * @param line The line to use for the test
 * @return Whether the test was successful
 * @note This implementation adds a and b and checks if the result is equal to the constant TARGET_SUM
 */
bool check_line_part_1 (Line_t line) {
	int occurences = 0;
	for (int i = 0; line.password[i] != '\0'; i++) {
		if (line.password[i] == line.character) {
			occurences++;
		}
	}
	return (line.min <= occurences) && (occurences <= line.max);
}
/**
 * Tests the two integers against the criterion in part 1 of puzzle 2 and returns whether it is fulfilled.
 *
 * @param a The first integer to use for the test
 * @param b The second integer to use for the test
 * @return Whether the test was successful
 * @note This implementation adds a and b and checks if the result is equal to the constant TARGET_SUM
 */
bool check_line_part_2 (Line_t line) {
#ifdef DEBUG
	printf("%s (%c) %d:%c, %d:%c\n", line.password, line.character, line.min, line.password[line.min - 1], line.max, line.password[line.max - 1]);
#endif
	return (line.password[line.min - 1] == line.character) ^ (line.password[line.max - 1] == line.character);
}

int main (int argc, char **argv) {
	if (argc != 2) {
		printf("Wrong number of arguments!\n");
		printf("Usage: %s INPUT_FILE\n", argv[0]);
		return -1;
	}

	FILE *file = fopen(argv[1], "r");
	if (file == NULL) {
		fprintf(stderr, "Could not open file %s!\n", argv[1]);
		return -2;
	}

	Line_t *lines = NULL;
	int n_lines = read_lines(file, &lines);
	fclose(file);

	if (n_lines < 1) {
		fprintf(stderr, "Could not read any lines from %s!\n", argv[1]);
		return -3;
	}

	int n_valid_passwords = 0;
	for (int i = 0; i < n_lines; i++) {
		if (check_line_part_1(lines[i])) {
			n_valid_passwords++;
		}
	}

	printf("Found %d valid passwords in part 1.\n", n_valid_passwords);

	n_valid_passwords = 0;
	for (int i = 0; i < n_lines; i++) {
#ifdef DEBUG
		if (check_line_part_2(lines[i])) {
			printf("Password number %d is valid!\n", i);
		} else {
			printf("Password number %d is invalid!\n", i);
		}
#endif
		if (check_line_part_2(lines[i])) {
			n_valid_passwords++;
		}
	}

	printf("Found %d valid passwords in part 2.\n", n_valid_passwords);

	free(lines);
	return 0;
}
