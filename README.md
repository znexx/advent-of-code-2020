# Advent of Code 2020
Having skipped the Advent of Code challenges previously, this year I found more motivation than usual and thus, you find here my attempts at solving the challenges!

## C
My main goal is to have all puzzels solved with standard C99 programs. If you have Make and GCC installed, running `make` in the directory of a puzzle should run my implementation for you.
